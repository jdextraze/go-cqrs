package cqrs

import (
	"github.com/satori/go.uuid"
)

type EventSourcing interface {
	Apply(interface{})
	LoadHistory([]*DomainEvent)
	UncommitedEvents() []*DomainEvent
	Id() uuid.UUID
	Name() string
	Version() int
}
