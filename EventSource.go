package cqrs

import "github.com/satori/go.uuid"

type EventSource struct {
	factory        AggregateFactory
	repository     AggregateRepository
	eventPublisher EventPublisher
}

func NewEventSource(
	factory AggregateFactory,
	repository AggregateRepository,
	eventPublisher EventPublisher,
) *EventSource {
	return &EventSource{
		factory:        factory,
		repository:     repository,
		eventPublisher: eventPublisher,
	}
}

func (es *EventSource) Create(name string, fn func(EventSourcing) error) error {
	agg, err := es.factory.Create(name, uuid.NewV4())
	if err != nil {
		return err
	}
	if err := fn(agg); err != nil {
		return err
	}
	if err := es.repository.Save(agg); err != nil {
		return err
	}
	return es.eventPublisher.PublishEvents(agg.UncommitedEvents())
}

func (es *EventSource) Update(name string, id uuid.UUID, fn func(EventSourcing) error) error {
	agg, err := es.factory.Create(name, id)
	if err != nil {
		return err
	}
	if err := es.repository.Load(agg); err != nil {
		return err
	}
	if err := fn(agg); err != nil {
		return err
	}
	if err := es.repository.Save(agg); err != nil {
		return err
	}
	return es.eventPublisher.PublishEvents(agg.UncommitedEvents())
}
