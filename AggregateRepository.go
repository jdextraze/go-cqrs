package cqrs

type AggregateRepository interface {
	Load(EventSourcing) error
	Save(EventSourcing) error
}
