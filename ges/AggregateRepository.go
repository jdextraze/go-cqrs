package ges

import (
	"bitbucket.org/jdextraze/go-cqrs"
	"bitbucket.org/jdextraze/go-gesclient"
	"encoding/json"
	"fmt"
)

type EventStoreAggregateRepository struct {
	eventStore   gesclient.Connection
	eventFactory cqrs.EventFactory
}

func NewEventStoreAggregateRepository(
	eventStore gesclient.Connection,
	eventFactory cqrs.EventFactory,
) *EventStoreAggregateRepository {
	return &EventStoreAggregateRepository{
		eventStore:   eventStore,
		eventFactory: eventFactory,
	}
}

func (r *EventStoreAggregateRepository) Load(es cqrs.EventSourcing) error {
	stream := fmt.Sprintf("%s-%s", es.Name(), es.Id())
	start := 0
	events := make([]*cqrs.DomainEvent, 0)
	for {
		streamEventsSlice, err := r.eventStore.ReadStreamEventsForward(stream, start, 200, nil)
		if err != nil {
			return err
		}
		for _, evt := range streamEventsSlice.Events() {
			event := evt.Event()
			eventObj, err := r.eventFactory.Create(event.EventType())
			if err != nil {
				return err
			}
			if err := json.Unmarshal(event.Data(), eventObj); err != nil {
				return err
			}
			events = append(events, cqrs.NewDomainEvent(
				es.Id(),
				event.EventId(),
				event.EventNumber(),
				eventObj,
				event.EventType(),
				event.Created(),
			))
		}
		if streamEventsSlice.IsEndOfStream() {
			break
		}
	}

	es.LoadHistory(events)

	return nil
}

func (r *EventStoreAggregateRepository) Save(es cqrs.EventSourcing) error {
	stream := fmt.Sprintf("%s-%s", es.Name(), es.Id())
	events := es.UncommitedEvents()
	expectedVersion := es.Version() - len(events) - 1
	eventsData := make([]*gesclient.EventData, len(events))
	for i, evt := range events {
		data, err := json.Marshal(evt.Event())
		if err != nil {
			return err
		}

		eventsData[i] = gesclient.NewEventData(
			evt.Id(),
			evt.EventType(),
			true,
			data,
			nil,
		)
	}

	res, err := r.eventStore.AppendToStream(stream, expectedVersion, eventsData, nil)
	if err != nil {
		return err
	}
	err = res.Error()
	if err != nil {
		return err
	}

	return nil
}
