package cqrs

import (
	"github.com/satori/go.uuid"
	"time"
)

type DomainEvent struct {
	aggregateId uuid.UUID
	id          uuid.UUID
	version     int
	event       interface{}
	eventType   string
	created     time.Time
}

func NewDomainEvent(
	aggregateId uuid.UUID,
	id uuid.UUID,
	version int,
	event interface{},
	eventType string,
	created time.Time,
) *DomainEvent {
	return &DomainEvent{aggregateId, id, version, event, eventType, created}
}

func (e *DomainEvent) AggregateId() uuid.UUID {
	return e.aggregateId
}

func (e *DomainEvent) Id() uuid.UUID {
	return e.id
}

func (e *DomainEvent) Version() int {
	return e.version
}

func (e *DomainEvent) Event() interface{} {
	return e.event
}

func (e *DomainEvent) EventType() string {
	return e.eventType
}

func (e *DomainEvent) Created() time.Time {
	return e.created
}
