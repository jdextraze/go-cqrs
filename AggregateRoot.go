package cqrs

import (
	"github.com/satori/go.uuid"
	"time"
)

type AggregateRoot struct {
	name             string
	id               uuid.UUID
	version          int
	uncommitedEvents []*DomainEvent
	applier          func(interface{}) string
}

func NewAggregateRoot(
	name string,
	id uuid.UUID,
	applier func(interface{}) string,
) *AggregateRoot {
	return &AggregateRoot{
		name:             name,
		id:               id,
		version:          0,
		uncommitedEvents: []*DomainEvent{},
		applier:          applier,
	}
}

func (a *AggregateRoot) Apply(evt interface{}) {
	eventType := a.applier(evt)
	a.uncommitedEvents = append(a.uncommitedEvents, NewDomainEvent(
		a.id,
		uuid.NewV4(),
		a.version,
		evt,
		eventType,
		time.Now().UTC(),
	))
	a.version++
}

func (a *AggregateRoot) LoadHistory(events []*DomainEvent) {
	for _, evt := range events {
		a.applier(evt.Event())
	}
}

func (a *AggregateRoot) UncommitedEvents() []*DomainEvent { return a.uncommitedEvents }

func (a *AggregateRoot) Name() string { return a.name }

func (a *AggregateRoot) Id() uuid.UUID { return a.id }

func (a *AggregateRoot) Version() int { return a.version }
