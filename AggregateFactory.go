package cqrs

import "github.com/satori/go.uuid"

type AggregateFactory interface {
	Create(name string, id uuid.UUID) (EventSourcing, error)
}
