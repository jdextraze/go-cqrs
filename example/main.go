package main

import (
	"bitbucket.org/jdextraze/go-cqrs"
	"bitbucket.org/jdextraze/go-cqrs/ges"
	"bitbucket.org/jdextraze/go-gesclient"
	"errors"
	"github.com/satori/go.uuid"
	"log"
)

func main() {
	conn := gesclient.NewConnection("192.168.22.10:1113")
	conn.WaitForConnection()

	agg := cqrs.NewEventSource(
		newAggregateFactory(),
		ges.NewEventStoreAggregateRepository(
			conn,
			newEventFactory(),
		),
		newEventPublisher(),
	)

	var id uuid.UUID
	if err := agg.Create("TestDomain", func(es cqrs.EventSourcing) (err error) {
		d := es.(*TestDomain)
		err = d.Create()
		id = d.Id()
		log.Println(len(d.UncommitedEvents()), d.Version(), d.Id(), d.Name())
		return
	}); err != nil {
		panic(err)
	}

	if err := agg.Update("TestDomain", id, func(es cqrs.EventSourcing) (err error) {
		d := es.(*TestDomain)
		err = d.Create()
		log.Println(len(d.UncommitedEvents()), d.Version(), d.Id(), d.Name())
		return
	}); err != nil {
		panic(err)
	}
}

// Event publisher

type nilEventPublisher struct{}

func newEventPublisher() *nilEventPublisher {
	return &nilEventPublisher{}
}

func (p *nilEventPublisher) PublishEvents(events []*cqrs.DomainEvent) error {
	return nil
}

// Aggregate factory

type aggregateFactory struct{}

func newAggregateFactory() *aggregateFactory {
	return &aggregateFactory{}
}

func (f *aggregateFactory) Create(name string, id uuid.UUID) (cqrs.EventSourcing, error) {
	switch name {
	case "TestDomain":
		return NewTestDomain(id), nil
	}
	return nil, errors.New("Unknown aggregate type")
}

// Event factory

type eventFactory struct{}

func newEventFactory() *eventFactory {
	return &eventFactory{}
}

func (f *eventFactory) Create(eventType string) (interface{}, error) {
	switch eventType {
	case "TestCreated":
		return &TestCreated{}, nil
	}
	return nil, errors.New("Unknown event type")
}

// Events

type TestCreated struct{}

// Domain

type TestDomain struct {
	cqrs.EventSourcing
	created bool
}

func NewTestDomain(id uuid.UUID) *TestDomain {
	d := &TestDomain{}
	d.EventSourcing = cqrs.NewAggregateRoot("TestDomain", id, d.testDomainEventsApplier)
	return d
}

func (d *TestDomain) testDomainEventsApplier(evt interface{}) string {
	switch evt.(type) {
	case *TestCreated:
		d.onCreated(evt.(*TestCreated))
		return "TestCreated"
	}
	return ""
}

func (d *TestDomain) Create() error {
	if d.created {
		return errors.New("Already created")
	}
	d.Apply(&TestCreated{})
	return nil
}

func (d *TestDomain) onCreated(event *TestCreated) {
	log.Println("onCreated")
	d.created = true
}
