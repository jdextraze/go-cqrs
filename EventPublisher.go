package cqrs

type EventPublisher interface {
	PublishEvents(event []*DomainEvent) error
}
