package cqrs

type EventFactory interface {
	Create(eventType string) (interface{}, error)
}
